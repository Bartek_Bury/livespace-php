<?php

namespace Livespace;

abstract class LivespaceAuthorize
{
    /**
     * @var Livespace|null
     */
    protected $_livespace = null;


    /**
     * @param Livespace $livespace
     */
    public function __construct(Livespace $livespace)
    {
        $this->_livespace = $livespace;
        $this->init();
    }

    public function init()
    {

    }

    /**
     * @param $action
     * @param $format
     * @param array $params
     * @return mixed
     */
    abstract public function call($action, $format, $params = array());
}

class LivespaceAuthorizeKey extends LivespaceAuthorize
{
    /**
     * @throws LivespaceException
     */
    public function init()
    {
        if (!$this->_livespace->getOption('api_key') || !$this->_livespace->getOption('api_secret')) {
            throw new LivespaceException('Options \'api_key\' and \'api_secret\' are required.');
        }
    }

    /**
     * @return mixed
     * @throws LivespaceException
     */
    protected function _getToken()
    {
        $result = $this->_livespace->_call('_Api/auth_call/_api_method/getToken', 'json', array(
            '_api_auth' => 'key',
            '_api_key' => $this->_livespace->getOption('api_key'),
        ));
        $result = LivespaceSerialize::factory('json')->unserialize($result);

        if (NULL === $result || !isset($result['data']) || !isset($result['data']['session_id']) || !isset($result['data']['token'])) {
            throw new LivespaceException('Cannot get token for authorization.');
        }
        return $result['data'];
    }

    /**
     * @param $token
     * @param $sessionId
     * @return array
     */
    protected function _getAuthParams($token, $sessionId)
    {
        return array(
            '_api_auth' => 'key',
            '_api_key' => $this->_livespace->getOption('api_key'),
            '_api_sha' => sha1($this->_livespace->getOption('api_key') . $token . $this->_livespace->getOption('api_secret')),
            '_api_session' => $sessionId,
        );
    }

    /**
     * @param $action
     * @param $format
     * @param array $params
     * @return mixed
     * @throws LivespaceException
     */
    public function call($action, $format, $params = array())
    {
        $tokenData = $this->_getToken();

        return $this->_livespace->_call($action, $format, array('data' => json_encode(array_merge($this->_getAuthParams($tokenData['token'], $tokenData['session_id']), $params))));
    }
}