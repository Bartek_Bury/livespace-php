## Installation

Requires PHP 5.3.

The recommended way to install is through [Composer](https://getcomposer.org):

First, install Composer:

```
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar require livespace/livespace-php
```

Finally, you can include the files in your PHP script:

```php
require "vendor/autoload.php";
```

## Examples

```php
use Livespace\Livespace;

$client = new Livespace(array(
    'api_url' => 'https://****.livespace.io',
    'api_key' => '*****',
    'api_secret' => '*****'
));

$contactData = array(
    'contact' => array(
        'firstname' => 'John',
        'lastname' => 'Smith',
        'company' => array(
            'name' => 'Company'
        )
    )
);

$result = $client->call('Contact/addContact', $contactData);
if ($result->getStatus()) {
    echo 'OK' . "\r\n";
    var_export($result->getResponseData());
} else {
    echo 'ERROR #' . $result->getResult()  . ":\r\n";
    print_r($result->getError());
}
```